<?php

namespace Drupal\unique_queue\Queue;

use \Drupal\Core\Queue\DatabaseQueue;

class QueueUnique extends DatabaseQueue {

  /**
   * @inheritdoc
   */
  public function createItem($data) {
    $query = $this->connection->select('queue','q');
    $query->addField('q','item_id');
    $query->condition('name',$this->name);
    $query->condition('data',serialize($data));
    $result = $query->execute()->fetchCol();
    if($result){
      return reset($result);
    }
    return parent::createItem($data);
  }
}
