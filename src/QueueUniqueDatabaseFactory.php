<?php

namespace Drupal\unique_queue;

use Drupal\Core\Queue\QueueDatabaseFactory;
use Drupal\unique_queue\Queue\QueueUnique;

/**
 * Factory for Queue Unique database queues.
 */
class QueueUniqueDatabaseFactory extends QueueDatabaseFactory {

  /**
   * {@inheritdoc}
   */
  public function get($name) {
    return new QueueUnique($name, $this->connection);
  }

}
