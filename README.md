# Unique Queue

Drupal is placing every item in queue.
It does not check if item is already in a queue.
Because of that there is a need to first check if item exists and then add it in a queue so that is reduces the need for executing twice for same item.


## Usage
By default Drupal is placing everything in DatabaseQueue.
If you want to move your queue to UniqueQueue add code below to your settings.php file:
```php
$settings['queue_service_your_queue_name'] = 'unique_queue.database';
```
